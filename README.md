# Postman Api Tests For Spotify

## Project consists of 2 parts:

### Part 1 represents GET, POST, PUT methods for Spofify API:
* Get a User's Profile https://developer.spotify.com/console/users-profile;
* Create a Playlist https://developer.spotify.com/console/post-playlists/;
* Get a Playlist https://developer.spotify.com/console/get-playlist/;
* Change a Playlist's Details https://developer.spotify.com/console/put-playlist/;
* Search for an Item https://developer.spotify.com/console/get-search-item/
### and include JavaScript Assertions (Snippets).

### Part 2 consists of Environment with variables.
